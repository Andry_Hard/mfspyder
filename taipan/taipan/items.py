# -*- coding: utf-8 -*-
import scrapy


class ProviderItem(scrapy.Item):
    name = scrapy.Field()


class HotelItem(scrapy.Item):
    h_art = scrapy.Field()
    name = scrapy.Field()
    h_type = scrapy.Field()
    prefecture = scrapy.Field()
    prefecture_name = scrapy.Field()
    area = scrapy.Field()
    area_name = scrapy.Field()
    coty = scrapy.Field()
    coty_name = scrapy.Field()
    city = scrapy.Field()
    city_name = scrapy.Field()
    dist = scrapy.Field()
    dist_name = scrapy.Field()
    address = scrapy.Field()
    geo_coord = scrapy.Field()
    num_of_rew = scrapy.Field()
    offers = scrapy.Field()
    longitude = scrapy.Field()
    latitude = scrapy.Field()
