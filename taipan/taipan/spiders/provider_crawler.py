import scrapy
from scrapy.spiders import CrawlSpider, Rule

from taipan.items import ProviderItem


class ProviderCrawlerSpider(CrawlSpider):
    name = "provider_cr"
    allowed_domains = ["https://www.tour.ne.jp"]
    
    def parse_item(self, response):
        i = ProviderItem()
        return i
