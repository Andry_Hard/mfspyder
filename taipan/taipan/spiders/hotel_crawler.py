import scrapy
import json
from scrapy.spiders import CrawlSpider, Rule

from taipan.items import HotelItem


class HotelCrawlerSpider(CrawlSpider):
    name = "hotel_cr"
    allowed_domains = ["https://www.tour.ne.jp"]
    start_urls = ["https://www.tour.ne.jp/api/json/j_hotel/parts_hotel_list/?limit=100&pg="+str(x) for x in range(260)]
    #"https://www.tour.ne.jp/api/json/j_hotel/"]

    def parse(self, response):
        jsonresponse = json.loads(response.text)
        hotels = jsonresponse['response']["hotel_info"]["hotel_list"]
        for hotel in hotels:
            i = HotelItem()
            i['h_art'] = hotel["hotel_id"]
            i['name'] = hotel["hotel_name"]
            i['prefecture'] = hotel["area_id"]
            i['prefecture_name'] = hotel["area_name"]
            i['dist'] = hotel.get("dist_id", None)
            i['dist_name'] = hotel.get("dist_name", None)
            i['city'] = hotel["city_id"]
            i['city_name'] = hotel["city_name"]
            i['coty'] = hotel["coty_id"]
            i['coty_name'] = hotel["coty_name"]
            i['address'] = hotel['address']
            i['longitude'] = hotel["longitude"]
            i["latitude"] = hotel["latitude"]
            yield i
