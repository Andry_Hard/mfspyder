FROM python:3.4
RUN mkdir /taipan
WORKDIR /taipan/taipan/
ADD . /taipan
RUN pip install PyMongo Scrapy geocoder
